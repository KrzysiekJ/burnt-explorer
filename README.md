# burnt-explorer

This unsophisticated application prints all [`OP_RETURN`](https://en.bitcoin.it/wiki/OP_RETURN) cryptocurrency transaction outputs in a given block range. Usage:

```
burnt-explorer <start height> <stop height>
```

It is assumed that you have `blackcoind` binary available in `$PATH`. If you want to use another command, set the `BLACKCOIND_COMMAND` environment variable.

Output format is [CSV (RFC 4180)](https://tools.ietf.org/html/rfc4180) without header, with minimal double quotes usage and with the following columns:

```
<block height>,<block hash>,<block time>,<transaction id>,<vout index>,<satoshis burnt>,<script hex>,<decoded script>
```

[`bitcoin-script` package](https://hackage.haskell.org/package/bitcoin-script) is used for decoding scripts. If you don’t have a full-fledged CSV parser, you may want to omit the last field, for example by piping the output through `cut -d , -f -7`.

For prebuilt binaries, see job artifacts in [CI builds](https://gitlab.com/KrzysiekJ/burnt-explorer/pipelines).

This repository contains also a synchronization script named `stool_fetcher` (run it with `--help` for more info).

This application uses [semantic versioning](http://semver.org/) 2.0.

## License

This software is licensed under under [the Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) (the “License”); you may not use this software except in compliance with the License. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.
